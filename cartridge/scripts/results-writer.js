/**
 * Helper object to output results into dev console 
 * 
 *
out.print()
out.println()
out.printXML()
out.printJSON() 
 */

const Logger = require('dw/system/Logger');

var ResultsWriter = function () {
	this.output = '';
}

ResultsWriter.prototype.print = function (value) {
	const stringValue = String(value);
	this.output += stringValue;  
	return stringValue;
};

ResultsWriter.prototype.println = function (value) {
	const stringValue = '&#10;' + String(value);
	this.output += stringValue;
	return stringValue;
};

ResultsWriter.prototype.printJSON = function (value) {
	var stringValue = '&#10;';
	try {
		stringValue += JSON.stringify(value);
	} catch (e) {
		Logger.error(e.message);
	}
	
	this.output += stringValue;
	return stringValue;
};


module.exports = new ResultsWriter();