'use strict';

/**
 * Controller for the "Development Console" page.
 *
 * @module controllers/DevConsole
 */
var txn = require('dw/system/Transaction');

/* Script Modules */
var app     = require('~/cartridge/scripts/app');
var guard   = require('~/cartridge/scripts/guard');
var out     = require('~/cartridge/scripts/results-writer.js');

/**
 * Renders the main page
 */
function renderStartPage() {
	app.getView().render('console-dev');
}

/**
 * Renders results on the main page
 */
function renderResults() {
	"use strict";
	var codeToEval = request.httpParameterMap.code.stringValue;
	var isTransactional = !!request.httpParameterMap.transaction.stringValue;
	try {
		if (isTransactional) {
			 txn.wrap(function(){
				 eval(codeToEval);
			 });
		} else {			
			eval(codeToEval);
		}
		app.getView({
			result: out.output,
			code: codeToEval,
			isTransactional: isTransactional ? 'checked' : ''
		}).render('console-dev');
		
	} catch (e) {
		app.getView({
			result: out.output, 
			code: codeToEval,
			isTransactional: isTransactional ? 'checked' : '',  
			error: e.message
		}).render('console-dev');
	}
}

/*
 * Web exposed methods
 */
/** @see module:controllers/StoreInventory~SetStore */
exports.Start = guard.ensure(['get'], renderStartPage);
exports.Exec = guard.ensure(['post'], renderResults);